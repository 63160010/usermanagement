/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.usermanagement;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author Acer
 */
public class UserService {
    
    public static ArrayList<User>userList = new ArrayList<>();
    static{
        userList.add(new User("admin", "password"));

        
    }
    

    public static boolean addUser(User user) {
        userList.add(user);
        return true;
    }
    public static boolean addUser(String userName,String password) {
        userList.add(new User(userName,password));
        return true;
    }
    public static boolean updateUser(int index,User user) {
        userList.set(index, user);
        return true;
    }
    
    public static User getUsers(int index) {
        if(index>userList.size()-1){
            return null;
        }
         return userList.get(index);
        
    }
    //all
    public static ArrayList<User> getUsers(){
         return userList;
        
    }
    //all
    public static ArrayList<User> searchUserName(String searchText){
        ArrayList<User> list =new ArrayList<>();
        for(User user: userList){
            if(user.getUserName().startsWith(searchText)){
                list.add(user);
            }
        }
         return list;
        
    }
    
    public static boolean delUser(int index) {
        userList.remove(index);
        return true;
    }
    public static boolean delUser(User user) {
        userList.remove(user);
  //      save();
        return true;
    }
    
    public static User Login(String userName,String password){
        for(User user: userList){
            if(user.getUserName().equals(userName) && user.getPassword().equals(password)){
                return user;
            }
        }
    return null;
    }
    
    public static void save(){
        FileOutputStream fos =null;
        try {
            File file =new File("users.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos =new ObjectOutputStream(fos);
            oos.writeObject(userList );
            
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            
        } catch (IOException ex) {
            
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                
            }
        }
    }
    public static void load(){
        FileInputStream fis = null;
        try {
            File file = new File("users.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            userList = (ArrayList<User>) ois.readObject();
            

            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {

        } catch (IOException ex) {
            
        } catch (ClassNotFoundException ex) {
            
        } finally {
            try {
                if(fis!=null){
                fis.close();
                }
            } catch (IOException ex) {
                
            }
        }
        System.out.println(userList);
    }
    
    
    
    
}
