/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.usermanagement;

/**
 *
 * @author Acer
 */
public class TestUserService {
    public static void main(String[] args) {
        //add
        UserService.addUser("user3","12345");
        System.out.println(UserService.getUsers());
        UserService.addUser(new User("user4","12345"));
        System.out.println(UserService.getUsers());
        
        //up
        User user =UserService.getUsers(4);
        System.out.println(user);
        user.setPassword("1234");
        UserService.updateUser(4, user);
        System.out.println(UserService.getUsers());
        
        //del
        UserService.delUser(user);
        System.out.println(UserService.getUsers());
        
        
        //login
        UserService.Login("adimin", "password");
    }
}
